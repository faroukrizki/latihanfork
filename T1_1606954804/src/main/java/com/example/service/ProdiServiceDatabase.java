package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.ProdiMapper;
import com.example.model.ProdiModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProdiServiceDatabase implements ProdiService {

	@Autowired
	private ProdiMapper prodiMapper;
	
	@Override
	public ProdiModel selectProdi(String kode_prodi) {
		log.info("select prodi with kode prodi {}", kode_prodi);
		return prodiMapper.selectProdi(kode_prodi);
	}

	@Override
	public List<ProdiModel> selectAllProdi() {
		log.info("select all prodi");
		return prodiMapper.selectAllProdi();
	}
	
}
