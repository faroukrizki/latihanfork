package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.model.UnivModel;
import com.example.service.PesertaService;
import com.example.service.ProdiService;
import com.example.service.UnivService;

@Controller
public class SBMPTNController {
	
	@Autowired
	PesertaService pesertaDAO;
	
	@Autowired
	ProdiService prodiDAO;
	
	@Autowired
	UnivService univDAO;
	
	@RequestMapping("/")
	public String goToHome(Model model){
		model.addAttribute("activeBar", "home");
		return "home";
	}
	
	@RequestMapping(value = "/pengumuman/submit", method = RequestMethod.POST)
    public String updateSubmit(Model model, PesertaModel peserta) {
		String nomor = peserta.getNomor();
        peserta = pesertaDAO.selectPeserta(nomor);
        if(peserta!=null){
        	if(peserta.getProdi()!=null){
        		peserta.setProdi(prodiDAO.selectProdi(peserta.getProdi().getKode_prodi()));
        	}
        	Integer usia = pesertaDAO.calculateAge(peserta.getTgl_lahir());
        	model.addAttribute("peserta", peserta);
        	model.addAttribute("usia", usia);
        	return "pengumuman";
        }
        model.addAttribute("peserta", null);
        model.addAttribute("nomor", nomor);
        return "pengumuman";
    }
	
	@RequestMapping("/univ")
	public String displayUniv(Model model){		
		List<UnivModel> universities = univDAO.selectAllUniv();
		model.addAttribute("universities", universities);
		model.addAttribute("activeBar", "univ");
		return "view-universities";
	}
	
	@RequestMapping("/univ/{kode_univ}")
	public String displayDetailUniv(Model model, @PathVariable(value="kode_univ") String kode_univ){
		UnivModel univ = univDAO.selectUniv(kode_univ);
		if(univ!=null){
			model.addAttribute("univ", univ);
			return "detil-universitas";
		}
		model.addAttribute("kode", kode_univ);
		return "detil-universitas";
	}
	
	@RequestMapping("/prodi")
	public String displayDetailProdi(Model model, @RequestParam(value="kode", required = false) String kode_prodi){
		ProdiModel prodi = prodiDAO.selectProdi(kode_prodi);
		if(prodi!=null){
			PesertaModel oldest= prodi.getParticipants().get(0);
			PesertaModel youngest = prodi.getParticipants().get(prodi.getParticipants().size()-1);
			Integer oldestAge = pesertaDAO.calculateAge(oldest.getTgl_lahir());
			Integer youngestAge = pesertaDAO.calculateAge(youngest.getTgl_lahir());
			model.addAttribute("prodi", prodi);
			model.addAttribute("oldest", oldest);
			model.addAttribute("youngest", youngest);
			model.addAttribute("oldestAge", oldestAge);
			model.addAttribute("youngestAge", youngestAge);
			return "detil-prodi";
		}
		model.addAttribute("kode", kode_prodi);
		return "detil-prodi";
	}
	
	@RequestMapping("/peserta")
	public String displayDetailPeserta(Model model, @RequestParam(value="nomor", required = false) String nomor){
		PesertaModel peserta = pesertaDAO.selectPeserta(nomor);
		if(peserta!=null){
			Integer usia = pesertaDAO.calculateAge(peserta.getTgl_lahir());
			model.addAttribute("peserta", peserta);
			model.addAttribute("usia", usia);
			return "detil-peserta";
		}
		model.addAttribute("nomor", nomor);
		return "detil-peserta";
	}
	
	
}
