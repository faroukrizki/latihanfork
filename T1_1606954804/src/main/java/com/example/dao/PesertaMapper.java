package com.example.dao;

import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;

import java.util.List;

@Mapper
public interface PesertaMapper {
	@Select("select * from peserta where nomor=#{nomor}")
	@Results(value = {
			@Result(property="nomor", column="nomor"),
			@Result(property="nama", column="nama"),
			@Result(property="tgl_lahir", column="tgl_lahir"),
			@Result(property="prodi", column="kode_prodi", one=@One(select = "selectProgram"))
	})
	PesertaModel selectPeserta(@Param("nomor") String nomor);
	
	@Select("select * from peserta")
	@Results(value = {
			@Result(property="nomor", column="nomor"),
			@Result(property="nama", column="nama"),
			@Result(property="tgl_lahir", column="tgl_lahir"),
			@Result(property="prodi", column="kode_prodi", one=@One(select = "selectProgram"))
	})
	List<PesertaModel> selectAllPeserta();
	
	@Select("select * from prodi where prodi.kode_prodi=#{kode_prodi}")
    ProdiModel selectProgram(@Param("kode_prodi") String prodi);
}
