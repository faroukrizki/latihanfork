package com.example.dao;

import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.example.model.ProdiModel;
import com.example.model.UnivModel;

import java.util.List;

@Mapper
public interface UnivMapper {
	@Select("select * from univ where kode_univ=#{kode_univ}")
	@Results(value={
			@Result(property="kode_univ", column="kode_univ"),
			@Result(property="nama_univ", column="nama_univ"),
			@Result(property="url_univ", column="url_univ"),
			@Result(property="programs", column="kode_univ", javaType=List.class, many=@Many(select="selectPrograms"))
	})
	UnivModel selectUniv(String kode_univ);
	
	@Select("select * from univ")
	@Results(value={
			@Result(property="kode_univ", column="kode_univ"),
			@Result(property="nama_univ", column="nama_univ"),
			@Result(property="url_univ", column="url_univ"),
			@Result(property="programs", column="kode_univ", javaType=List.class, many=@Many(select="selectPrograms"))
	})
	List<UnivModel> selectAllUniv();
	
	@Select("select * from univ limit #{param1}, #{param2}")
	@Results(value={
			@Result(property="kode_univ", column="kode_univ"),
			@Result(property="nama_univ", column="nama_univ"),
			@Result(property="url_univ", column="url_univ"),
			@Result(property="programs", column="kode_univ", javaType=List.class, many=@Many(select="selectPrograms"))
	})
	List<UnivModel> selectAllUnivLimited(Integer offset, Integer limit);
	
	@Select("select * from prodi where kode_univ=#{kode_univ}")
	List<ProdiModel> selectPrograms();
}
